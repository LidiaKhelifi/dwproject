SQL> select avg(unitssold) from actvars a, timelevel t
  2  where a.time_level=t.tid and t.YEAR_LEVEL='1995' and t.QUARTER_LEVEL in (' Q1','Q2') and
  3  t. MONTH_LEVEL in ('10','5','2') ;

AVG(UNITSSOLD)                                                                  
--------------                                                                  
    5,49454733                                                                  

SQL> select count(DOLLARCOST) from actvars a,timelevel t
  2  where a.time_level=t.tid and t.YEAR_LEVEL='1996'  and t.QUARTER_LEVEL in (' Q1','Q2') ;

COUNT(DOLLARCOST)                                                               
-----------------                                                               
          2916000                                                               


SQL> CREATE TABLE TimeLevel2(
  2  Tid        varchar(12) PRIMARY KEY NOT NULL,
  3  year_level      Number(4)  NOT NULL,
  4  Quarter_level   varchar(6) NOT NULL,
  5  month_level     Number(2),
  6  week_level      Number(2),
  7  day_level       Number(2))
  8  partition by range (Quarter_level)
  9  (partition p1 values ('Q1','Q2'),
 10  partition p1 values ('Q3','Q4'))
 11  TABLESPACE dim;
(partition p1 values ('Q1','Q2'),
                     *
ERREUR � la ligne 9 :
ORA-14007: absence de mot-cl� LESS 
SQL> CREATE TABLE TimeLevel2(
  2  Tid        varchar(12) PRIMARY KEY NOT NULL,
  3  year_level      Number(4)  NOT NULL,
  4  Quarter_level   varchar(6) NOT NULL,
  5  month_level     Number(2),
  6  week_level      Number(2),
  7  day_level       Number(2))
  8  PARTITION BY LIST(Quarter_level)
  9  (
 10  PARTITION p1 VALUES('Q1', 'Q2'),
 11  PARTITION p2 VALUES('Q3', 'Q4'))
 12  TABLESPACE dim;

Table cr��e.

SQL> alter table TimeLevel2
  2  exchange partition p1 , partition p2
  3  with table timelevel
  4  without validation
  5   update global indexes;
exchange partition p1 , partition p2
                      *
ERREUR � la ligne 2 :
ORA-00970: mot-cl� WITH absent 


SQL> alter table TimeLevel2
  2  exchange partition p1
  3  exchange partition p2
  4  with table timelevel
  5  without validation
  6   update global indexes;
exchange partition p2
*
ERREUR � la ligne 3 :
ORA-00970: mot-cl� WITH absent 


SQL> alter table TimeLevel2
  2  exchange partition p1
  3  with table timelevel
  4  without validation
  5   update global indexes;
with table timelevel
           *
ERREUR � la ligne 3 :
ORA-02266: les cl�s primaires/uniques de la table sont r�f�renc�es par des cl�s 
�trang�res 


SQL> spool off;
